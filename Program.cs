﻿using System;
using System.Diagnostics;
using System.IO;

namespace KraskalAlgorithm
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var inputDataSizes = new List<int>();
            var rand = new Random();
            var stopwatch = new Stopwatch();
            var inputValuesPath = @"C:\Users\2695\source\repos\KraskalAlgorithm\KraskalAlgorithm\inputValues.txt";
            if (File.Exists(inputValuesPath)) File.Delete(inputValuesPath);
            using (StreamWriter sw = File.CreateText(inputValuesPath))
            {
                for (int i = 0; i < 100; i++)
                {
                    int inputDataSize = 0;
                    int countVertexes = (int)rand.Next(1, 30);
                    sw.WriteLine(countVertexes);
                    for (int j = 0; j < countVertexes * 2; j++)
                    {
                        inputDataSize++;
                        int weigth = (int)rand.Next(20);
                        int source = (int)rand.Next(countVertexes);
                        int destination = (int)rand.Next(countVertexes);
                        if (source == destination) weigth = 0;
                        sw.WriteLine("{0}, {1}, {2}", weigth, source, destination);
                    }
                    if (i != 100) sw.WriteLine();
                    inputDataSizes.Add(inputDataSize);
                }
            }
            string inputValuesText;
            using (StreamReader sr = File.OpenText(inputValuesPath))
            {
                inputValuesText = File.ReadAllText(inputValuesPath);
            }

            var graphsInfo = inputValuesText.Split("\r\n\r\n");

            var timesPath = @"C:\Users\2695\source\repos\KraskalAlgorithm\KraskalAlgorithm\times.txt";
            if (File.Exists(timesPath)) File.Delete(timesPath);
            using (StreamWriter sw = File.CreateText(timesPath))
            {
                for (int i = 0; i < graphsInfo.Length; i++)
                {
                    var graphMas = graphsInfo[i].Split("\n");
                    if (graphMas[0] != "")
                    {
                        var count = int.Parse(graphMas[0]);

                        var edges = new List<Graph.Edge>();
                        for (int j = 1; j < graphMas.Length; j++)
                        {
                            int weight = int.Parse(graphMas[j].Split(", ")[0]);
                            int source = int.Parse(graphMas[j].Split(", ")[1]);
                            int destination = int.Parse(graphMas[j].Split(", ")[2]);
                            edges.Add(new(weight, source, destination));
                        }
                        var graph = new Graph(count, edges.ToArray());
                        stopwatch.Start();
                        var ostov = graph.KraskalAlgorithm();
                        stopwatch.Stop();
                        sw.WriteLine("{0} data size, {1} iterations, {2} milliseconds", inputDataSizes[i], ostov.Item2, stopwatch.ElapsedMilliseconds);
                        stopwatch.Restart();
                    }
                }
            }

        }
    }
    public class Graph
    {
        public int iterationsCount = 0;
        public int TreeWeight { 
            get 
            {
                int treeWeigth = 0;
                foreach (var edge in Edges)
                {
                    treeWeigth += edge.Weight;
                }
                return treeWeigth;
            }
        }
        public int CountVertex;
        public List<List<int>> VertexList = new List<List<int>>();
        public Edge[] Edges;
        public class Edge
        {
            public int Weight { get; }
            public int Source { get; }
            public int Destination { get; }
            public Edge(int w, int s, int d)
            {
                Weight = w;
                Source = s;
                Destination = d;
            }
        }
        public Graph(int countV, params Edge[] edges)
        {
            Edges = edges;
            CountVertex = countV;
            for (int i = 0; i < countV; i++)
            {
                VertexList.Add(new List<int>() { i });
            }
        }
        public List<Edge> SortWeight(Edge[] edges)
        {
            return edges.OrderBy(e => e.Weight).ToList(); 
        }
        public Tuple<Graph, int> KraskalAlgorithm()
        {
            int newtreeweigth = 0;
            List<Edge> OstovEgdes = new();
            var result = SortWeight(Edges); // O(N * logN)
            foreach (var edge in result) // O(N)
            {
                iterationsCount++;
                int source = edge.Source;
                int destination = edge.Destination;
                int idxOfSource = VertexList.IndexOf(VertexList.SingleOrDefault(x => x.Contains(source))!);
                int idxOfDestination = VertexList.IndexOf(VertexList.SingleOrDefault(x => x.Contains(destination))!);
                if (idxOfDestination != idxOfSource)
                {
                    OstovEgdes.Add(edge);
                    VertexList[idxOfSource].AddRange(VertexList[idxOfDestination]);
                    VertexList.RemoveAt(idxOfDestination);
                    newtreeweigth += edge.Weight;
                }
                if (VertexList.Count == 1) break;
            } 
            if (VertexList.Count != 1)
            {
                return new Tuple<Graph, int>(new Graph(0), iterationsCount);
            }
            return new Tuple<Graph, int>(new Graph(CountVertex, OstovEgdes.ToArray()), iterationsCount);
        }
    }
}